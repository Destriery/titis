
import './libs/select2/js/select2.js';
import './libs/slick/slick';
import './libs/jquery.accordion';
import './libs/jquery.imresize';
import './libs/jquery.tinyscrollbar';

//** polyfill for <use xlink:href="symbol-defs.svg#icon-edit">
//import 'svgxuse';

import './main';

if ($('.page__chat').length) {
    $('.chat__messages__items__wrapper').tinyscrollbar();
    $('.chat__contacts__items__wrapper').tinyscrollbar();
    var chatScroll = $('.chat__messages__items__wrapper').data('plugin_tinyscrollbar');
    chatScroll.update('bottom');
}

$('.select__wrapper select').select2({
    minimumResultsForSearch: Infinity
});

$('.meeting__faq').accordIon({
    tabs_class : '> ul > li:has(.hidden) a', //.link
    blocks_class : '> ul > li .hidden', //.container
	indicators_class : '> ul > li:has(.hidden) a', //.turn
});

function slicks () {
    function slickUserComments () {
        var userComments = $('.user__info__comments');
        userComments.find('.user__info__comments__body').slick({
            arrows: true,
            dots: false,
            fade: false,
            cssEase: 'linear',
            autoplay: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            rows: 1,
            prevArrow: userComments.find('.user__info__comments__arrow--left'),
            nextArrow: userComments.find('.user__info__comments__arrow--right')
        });
        var commentsSlick = userComments.find('.user__info__comments__body').slick('getSlick');
        userComments.find('.count-comments').text(commentsSlick.slideCount);

        userComments.find('.user__info__comments__body').on('afterChange', function(event, slick, currentSlide){
            $(this).parent().find('.current-comment').text(currentSlide+1);
        });
    }
    
    function slickUserGallery() {
        var userGallery = $('.user__photo-gallery');

        if ( window.innerWidth <= 950 ) {
            setTimeout(function () {
                userGallery.slick({
                    arrows: false,
                    dots: true,
                    fade: false,
                    cssEase: 'linear',
                    autoplay: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    rows: 1,
                    slide: 'a.user__photo-gallery__item'
                });
                userGallery.data('slick',1);
                //$('.user__photo-gallery a.user__photo-gallery__item img').imResize('destructor');
                $('.user__photo-gallery a.user__photo-gallery__item img').imResize({method : 'field'});
            }, 200);
        }
        else {
            if (userGallery.data('slick')) {
                userGallery.slick('unslick');
                userGallery.data('slick',0);
            }
            $('.user__photo-gallery a.user__photo-gallery__item img').imResize();
        }
        
    }
    
    if (  $('.user__info__comments').length ) {
        slickUserComments();
    }
    
    if (  $('.page__user--view .user__photo-gallery').length ) {
        slickUserGallery();
        $(window).resize(slickUserGallery);
    }
}
slicks();

