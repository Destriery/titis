(function($){
    /* main */
    function resizeMainHeader() {
        var windowHeight = $(window).height();
        var headerPadding = parseInt($('.header--main').css('padding-bottom'));
        var headerMainHeight = $('.header__main').height();
        var headerInnerHeight = $('.header__inner').height();
        var headerArrowDownHeight = $('.header__main__arrow-down').height();
        
        if (headerMainHeight + headerInnerHeight + headerArrowDownHeight + headerPadding < windowHeight) {
            $('.header--main').css({ 
                height : windowHeight - headerPadding
            });
            $('.header__main').css({
                paddingTop : (windowHeight - (headerMainHeight + headerInnerHeight + headerArrowDownHeight))/2
            })
        }
        
    }
    if ( $('.header--main').length ) resizeMainHeader(); 
    
    
    function moveToElem(elem) {
        $('html, body').animate({ scrollTop: $(elem).offset().top }, 300);
    }
    $('.header__main__arrow-down').on('click', function () {
        moveToElem( $('.main-about') );
    });
    
    /* open header menu */
    $('.header__menu, .header__city__list').on('click', '.icon__nav, .header__city__current ', function () {
        var ul = $(this).siblings('ul');
        
        if (ul.hasClass('open')) {
            ul.slideUp();
        }
        else {
            ul.slideDown();
        }
        ul.toggleClass('open');
    });
    $('.header__user__profile').on('click', function () {
        var ul = $(this).find('ul');
        
        if (ul.hasClass('open')) {
            ul.slideUp();
        }
        else {
            ul.slideDown();
        }
        ul.toggleClass('open');
    });
    
    
    function openTab (tabClass, hiddenClass) {
        
        var hidden = $(this).siblings('.'+hiddenClass);
        
        if (!hidden.length) return;
        if (!hidden.children().length) return;
        
        if ( $(this).hasClass(tabClass+'--open') ) {
            hidden.slideUp();
        }
        else {
            hidden.slideDown();
        }
        
        $(this).toggleClass(tabClass+'--open');
        
    }
    /* my meetings man */
    $('.my-meetings__items').on('click', '.my-meetings__item:not(.open) .my-meetings__item__status', function () {       
        openTab.call(this, 'my-meetings__item__status', 'my-meetings__item__responses');       
    }); 
    /* meeting create */
    $('.meeting__additional-params--create').on('click', '.meeting__additional-params__title', function () {       
        openTab.call(this, 'meeting__additional-params__title', 'meeting__additional-params__content');       
    });
    
    /* show input in form-field__field */
    $('.meeting__fields__place input[type="radio"]').on('change', function () {
        $(this).parents('.meeting__fields__place').find('.hidden').hide();
        $(this).parents('.form-field__field__item__row').find('.hidden').show();
    });
    
    /* show/hide filter on tablet */
    $('.control-bar__buttons__show-filter').on('click', function () {
        $(this).toggleClass('button--default--black');
        
        var show = $(this).hasClass('button--default--black');
        
        if (show) {           
            $(this).parents('.control-bar').find('.control-bar__block').show();            
        }
        else {
            $(this).parents('.control-bar').find('.control-bar__block').hide();  
        }
        
    });
    
    
    /* edit switch */
    $('.user__tabs__content__item__header').on('click', '.icon__wrapper', function () {
        var editSwitch = $(this).hasClass('active');
        
        $(this)
            .toggleClass('active')
            .parents('.user__tabs__content__item').toggleClass('user__tabs__content__item--edit')
            .find('.form-field__field').toggleClass('disabled')
            .find('input, select').prop('disabled', editSwitch);
        
    });
    
    /* services tab (user_view sidebar) */
    function servicesTabs() {
        
        function setHeight () {
            var sidebarTags = $('.user__info-sidebar__tags');
            
            if ( $(window).width() <= 950 ) {
                var padding = 50;
                var defaultHeight = sidebarTags.height();
                
                var heightCurContent = $('.user__info-sidebar__tags__item__title.active + .user__info-sidebar__tags__item__content').height();
                sidebarTags.css({'height' : heightCurContent + padding});
            }
            else {
                sidebarTags.css({'height' : 'auto'});
            }
        }
        
        setHeight();
        $(window).resize(setHeight);

        $('.user__info-sidebar__tags__item__title').on('click', function () {
            $('.user__info-sidebar__tags__item__title').removeClass('active');
            $(this).addClass('active');
            setHeight();
        });
    }
    if ( $('.user__info-sidebar__tags').length ) servicesTabs();
    
    $('.user__photo-gallery__item__menu').on('click', function () {
        var $this = $(this);
        var $this_popup = $this.find('.popup__content');
        
        $this.css({opacity : 1});
        $this_popup.css({opacity : 1, visibility : 'visible'});
        
        var close = false;
        $('body').on('click.popup__content', function () {
            if (close) {
                $this.removeAttr('style');
                $this_popup.css({opacity : 0, visibility : 'hidden'});
                
                $('body').off('click.popup__content');
            }
            else {
                close = true;
            }
        });
        
    });
    
    function chat () {
        var chat = $('.chat');
        var chatItems = $('.chat__contact__item');
        var chatClose = $('.chat__header__back');
        
        chatItems.on('click', function () {
            chat.toggleClass('open');
            
            chatItems.removeClass('active');
            $(this).addClass('active');
            
            $('.chat__contacts__items__wrapper').tinyscrollbar();
            var chatScroll = $('.chat__messages__items__wrapper').data('plugin_tinyscrollbar');
            chatScroll.update('bottom');
        });
        chatClose.on('click', function () {
            chat.toggleClass('open');
        });
    }
    if ($('.chat').length) chat();
    
    
    function openPageOnDesktop() {
        
        /* user edit tabs */
        $('.user__tabs').on('click', '.user__tabs__titles__item', function () {
            
            var index = $(this).index();
            
            $(this).siblings().removeClass('active');
            $(this)
                .addClass('active')
                .parents('.user__tabs').find('.user__tabs__content__item').hide()
                .eq(index).show();
             
            var curContent = $(this).parents('.user__tabs').find('.user__tabs__content__item').eq(index);
            if (curContent.find('.user__photo-gallery')) curContent.find('.user__photo-gallery__item__img__wrapper img').imResize();
            
        });
    }
    
    function openPageOnMobile () {
        function meetings () {
            var page = $('.page__my-meetings--girl, .page__my-meetings--man');
            var myMeetingsItems = page.find('.my-meetings__item');
            var pageClose = page.find('.my-meetings__header--back .icon__arrow-left');
            
            myMeetingsItems.on('click.page_open', function () {
                
                if (!$(this).hasClass('open')) {
                    page.toggleClass('open-one'); 
                    $(this).toggleClass('open');
                    
                    initGallery.call(this);
                }
            });
            
            pageClose.on('click.page_close', function () {
                page.toggleClass('open-one');
                $('.my-meetings__item.open').toggleClass('open');
            });
            
            function initGallery () {
                for (var i = 0; i < $(this).find('.my-meetings__item__responses__item').length; i++) {
                    
                    var itemsInner = $(this).find('.my-meetings__item__responses__item').eq(i).find('.my-meetings__item__responses__item__gallery__inner');
                    var itemImg = $(this).find('.my-meetings__item__responses__item').eq(i).find('.my-meetings__item__responses__item__gallery__item');
                    var count = itemImg.length;
                    var width = itemImg.width() + parseInt(itemImg.css('margin-left')) + parseInt(itemImg.css('margin-right'));
                    
                    itemImg.find('img').imResize();
                    
                    itemImg.parent().css({'width' : width*count});
                    
                    itemsInner.tinyscrollbar({ axis: "x"});
                }
            } 
        }
        
        function userEdit () {
            var page = $('.page__user--edit');
            var initEditButton = page.find('.control-bar .icon__edit--big');
            var userEditTabsTitles = page.find('.user__tabs__titles__item');
            var userEditTabsContent = page.find('.user__tabs__content__item');
            var tabClose = page.find('.user__tabs__content__item__header--back .icon__arrow-left');
            var pageClose = page.find('.user__header--back .icon__arrow-left');
            
            initEditButton.on('click.edit_open', function () {
                page.toggleClass('page__user--edit--open'); 
            });
            
            userEditTabsTitles.on('click.edit_open_tab', function () {
                var index = $(this).index();
                $(this).siblings().removeClass('active');
                $(this)
                    .addClass('active')
                    .parents('.user__tabs').find('.user__tabs__content__item').hide()
                    .eq(index).show();
                
                
                userEditTabsContent.eq(index).find('input, select').prop('disabled',false);
                
                userEditTabsContent.eq(index).find('label').on('click.edit_open_tab__focus', function () {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');
                });
                
                userEditTabsContent.eq(index).filter('.user__tabs__content__item--table').find('.form-field__container').on('click.edit_open_tab__table', function () {
                    
                    userEditTabsContent.eq(index).filter('.user__tabs__content__item--table').find('.form-field__container').removeClass('active');
                    $(this).addClass('active');
                    
                    userEditTabsContent.find('.user__tabs__content__item__header--back--table span').text($(this).find('.form-field__title').text());
                    
                    page.addClass('page__user--edit--open--table');
                    
                    userEditTabsContent.find('.user__tabs__content__item__header--back--table .icon__arrow-left').on('click.edit_close_tab__table', function () {
                        userEditTabsContent.eq(index).filter('.user__tabs__content__item--table').find('.form-field__container').removeClass('active');
                        
                        page.removeClass('page__user--edit--open--table');
                        
                        $(this).off('click.edit_close_tab__table');
                    });
                    
                });

                page.toggleClass('page__user--edit--open--submenu'); 
                
                /* init gallery */
                var gallery = userEditTabsContent.eq(index).find('.user__photo-gallery');
                if (gallery) initGallery(gallery);
            });
            
            tabClose.on('click.edit_close_tab', function () {
                userEditTabsContent.find('input').off('click.edit_open_tab__focus');
                
                page.toggleClass('page__user--edit--open--submenu');
            });
            
            pageClose.on('click.edit_close', function () {
                page.toggleClass('page__user--edit--open'); 
            });
            
            function initGallery (gallery) {
                var itemImg = gallery.find('.user__photo-gallery__item:not(.user__photo-gallery__add-photo)');
            
                var width = itemImg.width() + parseInt(itemImg.css('margin-left')) + parseInt(itemImg.css('margin-right')),
                    count = itemImg.length;
                    
                itemImg.find('img').imResize();
                
                itemImg.parent().css({'width' : width*count});

                gallery.tinyscrollbar({ axis: "x"});
                
                itemImg.find('.user__photo-gallery__item__img__wrapper').on('click.user_edit_open_img', function () {
                    $this = $(this);
                    
                    var overlay = $('.user__overlay-photo');
                    
                    overlay.find('.user__photo-gallery__item__img__wrapper').html( $(this).find('img').eq(0).outerHTML ).show();
                    if ($(this).parents('.user__photo-gallery__item').hasClass('user__photo-gallery__item--ava')) {
                        overlay.addClass('user__photo-gallery__item--ava');
                    }
                    
                    page.toggleClass('page__user--edit--open--img');
                    
                    overlay.find('.user__photo-gallery__item__img__wrapper img').imResize({method : 'field'});
                    
                    /* close */
                    overlay.find('.user__photo-gallery__item__back .icon').on('click.user_edit_close_img', function () {
                        $(this).off('click.user_edit_close_img');
                        
                        overlay.removeClass('user__photo-gallery__item--ava');
                        
                        page.toggleClass('page__user--edit--open--img');
                    });
                });
            
            }
        }
        
        if ($('.page__my-meetings--girl, .page__my-meetings--man').length  && $(window).width() <= 480) meetings();
        if ($('.page__user--edit').length && $(window).width() <= 650) userEdit();
    }
    
    if ( $(window).width() > 480 && $(window).width() <= 650 ) openPageOnMobile();  
    else if ( $(window).width() <= 480 ) openPageOnMobile();
    else openPageOnDesktop();
    
    
    /* modals */
    modals();
    function modals() {
        var body = $('body');
        var overlay = $('.overlay');
        var openModal = $('.open-modal');
        var modals = $('.modal');
        
        // show/hide
        openModal.on('click', function () {
            var id = $(this).data('modal-id');
            hideModal();
            showModal(id);
        });
        
        $('.modal__close').on('click', hideModal);
        overlay.on('click', function (event) {
            if (event.target == overlay[0]) {
                hideModal();
            }
        });
        
        //valid
        modals.find('form').on('submit', checkValid);
        modals.find('input, textarea').on('focus', function () {
            $(this).removeClass('input__error')
                .parents('.modal').find('.modal__error').removeClass('show');
        });
        
        
        
        function showModal(id) {
            $('#'+id).css({display: 'block'});
            overlay.fadeIn();
            if (!!$.fn.select2) {
                $('#'+id).find('select').select2({             
                    minimumResultsForSearch: Infinity
                });
            }
            body.css({overflow: 'hidden'});
        }
        
        function hideModal() {
            overlay.fadeOut();
            modals.fadeOut();
            body.css({overflow: 'auto'});
        } 
        
        function checkValid (event) {
            var $this = $(this);
            var requiredFields = ['callback_phone'];
            var error = false;
            
            requiredFields.forEach(function (item, i) {
                var elem = $this.find('[name="'+item+'"]');
                if (!!elem.length) {
                    if ( !elem.val() ) {
                        elem.addClass('input__error');
                        $this.find('.modal__error').addClass('show');
                        error = true;
                    }
                }
            });
            
            if (error) {
                event.preventDefault(); 
                (event.cancelBubble) ? event.cancelBubble : event.stopPropagation;
            }
            
        }
    }
    
})($);
